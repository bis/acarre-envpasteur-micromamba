From docker.io/mambaorg/micromamba

# update the OS
USER root
RUN	apt-get update && \
	apt-get -y install curl && \
	DEBIAN_FRONTEND=noninteractive apt-get -y upgrade && \
	DEBIAN_FRONTEND=noninteractive apt-get -y install libtiff5 && \
	DEBIAN_FRONTEND=noninteractive apt-get -y autoremove && \
	DEBIAN_FRONTEND=noninteractive apt-get -y clean autoclean && \
	export MAMBA_ROOT_PREFIX=/opt/conda && \
        eval "$(micromamba shell hook -s posix )"  && \
        micromamba update -q -y micromamba -c conda-forge -p ${MAMBA_ROOT_PREFIX} && \
        micromamba update -q -y --all -c conda-forge -p ${MAMBA_ROOT_PREFIX} && \
	micromamba install -y \
		--name base \
		-c rdkit \
		-c openbabel \
		-c conda-forge \
		-c acellera \
		-c bioconda \
		-c pytorch \
		-c nvidia \
		python=3 \
		biopandas \
		bs4 \
		jupyter \
		jupyterlab \
		matplotlib \
		mgltools \
		moleculekit \
		numpy \
		openbabel \
		pandas \
		pymol-open-source \
		pytorch \
		pytorch-cuda=11.6 \
		rdkit \
		scikit-learn \
		scipy \
		seaborn && \
	micromamba clean --all -y

ADD https://gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba/-/raw/main/runscript.sh /runscript.sh
RUN chmod 755 /runscript.sh

ENTRYPOINT [ "/runscript.sh"]
