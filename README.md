[![pipeline status](https://gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba/badges/main/pipeline.svg)](https://gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba/-/commits/main) 

## installing singularity container with moleculekit(require a glibc newer than CentOS-7 glibc-2.17-326.el7_9.x86_64) pytorch 1.3.1 cuda 11.7 micromamba version

- Singularity: complete Singularity recipe for CI
`singularity build Singularity.sif Singularity` (might need `sudo`)


## singularity conversion if you have singularityCE or apptainer installed

```
singularity build acarre-EnvPasteur-micromamba.sif  oras://registry-gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba:latest
```
alternative with docker registry
```
singularity build acarre-EnvPasteur-micromamba.sif  docker://registry-gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba:main
```

## on Maestro.pasteur.fr

```
module add apptainer
apptainer build acarre-EnvPasteur-micromamba.sif  oras://registry-gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba:latest
```

alternative with docker registry
```
apptainer build acarre-EnvPasteur-micromamba.sif  docker://registry-gitlab.pasteur.fr/bis/acarre-envpasteur-micromamba:main
```
